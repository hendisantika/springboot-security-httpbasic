# Spring Boot Security HTTP Basic
## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-security-httpbasic.git`
2. Go to the folder: `cd springboot-security-httpbasic`.
3. Run the application: `mvn clean spring-boot:run`.
4. Go to your favorite browser: http://localhost:8080/index
5. Use these following username & password:
    | # | username | password |
    | --- | --- | --- |
    | 1 | admin | admin123 |
    | 2 | user | user123 |
    | 3 | manager | manager123 |
     
## Screen shot

Login Page

![Login Page](img/login.png "Login Page")

Index Page

![Index Page](img/index.png "Index Page")

Home Page

![Home Page](img/home.png "Home Page")

Admin Page

![Admin Page](img/admin.png "Admin Page")

Management Page

![Management Page](img/management.png "Management Page")

