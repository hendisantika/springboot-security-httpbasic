package com.hendisantika.springbootsecurityhttpbasic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-httpbasic
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/03/20
 * Time: 11.38
 */
@Controller
@RequestMapping("admin")
public class AdminController {

    @GetMapping("index")
    public String index() {
        return "admin/index";
    }
}