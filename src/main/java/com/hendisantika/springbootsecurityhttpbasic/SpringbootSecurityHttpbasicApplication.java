package com.hendisantika.springbootsecurityhttpbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootSecurityHttpbasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityHttpbasicApplication.class, args);
    }

}
